﻿using System;

namespace NameTask
{
    class Program
    {
        static void Main(string[] args)
        {

            //Task 1
            Console.WriteLine("Hello! My name is Mikail");

            //Tastk 2
            Console.WriteLine("Please enter your name in the line below");

            String name = Console.ReadLine();
            String correctName = name.Replace(" ", String.Empty);

            Console.WriteLine($"Hello {name}, your name is {correctName.Length} characters long and starts with the letter {correctName[0]}");
        }
    
    }
}
